package view;

import android.content.Context;

import androidx.core.content.res.ResourcesCompat;

import com.example.myapplication.R;

import java.util.ArrayList;
import java.util.List;

import datamodel.Cloth;
import datamodel.Post;

public class DataFakeGenerator {

    public static List<Post> getData(Context context) {
        List<Post> Posts = new ArrayList<>();
        for (int i = 0; i < 6; i++) {
            Post post = new Post();
            post.setId(i);
            post.setTitle("لورم ایپسوم متن ساختگی ");
            post.setContent("لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد");
            post.setDate("2ساعت پیش");
            switch (i) {
                case 1:
                    post.setPostImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.pic1, null));
                    break;
                case 2:
                    post.setPostImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.pic2, null));
                    break;
                case 3:
                    post.setPostImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.pic3, null));
                    break;
                case 4:
                    post.setPostImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.pic4, null));
                    break;
                case 5:
                    post.setPostImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.pic5, null));
                    break;
                case 6:
                    post.setPostImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.pic6, null));
                    break;
            }
            Posts.add(post);
        }
        return Posts;
    }

    public static List<Cloth> getClothes(Context context) {
        List<Cloth> cloths = new ArrayList<>();
        for (int i = 1; i <= 8; i++) {
            Cloth cloth = new Cloth();
            cloth.setId(i);
            cloth.setTitle("لورم ایپسوم متن ساختگی ");
            cloth.setViewcount(700);
            switch (i) {
                case 1:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_a, null));
                    break;
                case 2:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_b, null));
                    break;
                case 3:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_c, null));
                    break;
                case 4:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_d, null));
                    break;
                case 5:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_e, null));
                    break;
                case 6:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_f, null));
                    break;
                case 7:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_h, null));
                    break;
                case 8:
                    cloth.setImage(ResourcesCompat.getDrawable(context.getResources(), R.drawable.photo_k, null));
                    break;
            }

            cloths.add(cloth);
        }
        return cloths;
    }
}
