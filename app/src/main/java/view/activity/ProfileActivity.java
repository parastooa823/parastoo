package view.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.myapplication.R;

public class ProfileActivity extends AppCompatActivity {

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        ImageButton btnBack = (ImageButton) findViewById(R.id.back_button);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });
        Button editAvatar = (Button) findViewById(R.id.edit_avatar);
        editAvatar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ProfileActivity.this, "ویرایش عکس کلیک شد", Toast.LENGTH_LONG).show();
            }
        });

        EditText nameEditText = (EditText) findViewById(R.id.edittext_firstname);

        EditText lastNameText = (EditText) findViewById(R.id.edittext_lastname);

        nameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Toast.makeText(ProfileActivity.this, charSequence.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Toast.makeText(ProfileActivity.this, charSequence.toString(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Toast.makeText(ProfileActivity.this, editabl.toString(),Toast.LENGTH_LONG).show();
            }
        });
        CheckBox JavaCheckBox = (CheckBox) findViewById(R.id.java_checkbox);
        CheckBox cssCheckBox = (CheckBox) findViewById(R.id.css_checkbox);
        CheckBox htmlCheckBox = (CheckBox) findViewById(R.id.html_checkbox);

        JavaCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(ProfileActivity.this, String.valueOf(b), Toast.LENGTH_SHORT).show();


            }
        });

        RadioButton maleRadio = (RadioButton) findViewById(R.id.male_radio);
        RadioButton femaleRadio = (RadioButton) findViewById(R.id.female_radio);

        maleRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Toast.makeText(ProfileActivity.this, String.valueOf(b), Toast.LENGTH_SHORT).show();

            }
        });

        final Button saveform = (Button) findViewById(R.id.save_form);
        saveform.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(ProfileActivity.this, "save form is clicked", Toast.LENGTH_SHORT).show();

            }
        });


    }
}
