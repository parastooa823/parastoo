package view.activity;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

import adapter.NewsAdapter;
import view.DataFakeGenerator;

public class LastNewsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_news);

        RecyclerView recyclerView=(RecyclerView)findViewById(R.id.recycler_view);
        NewsAdapter newsAdapter=new NewsAdapter(this, DataFakeGenerator.getData(this));
        recyclerView.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));
        recyclerView.setAdapter(newsAdapter);

    }
}
