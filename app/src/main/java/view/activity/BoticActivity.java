package view.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.TableLayout;

import com.example.myapplication.R;
import com.google.android.material.tabs.TabLayout;

import adapter.ClothesViewPagerAdapter;

public class BoticActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_botic);

        ViewPager viewPager=(ViewPager)findViewById(R.id.viewpager);
        ClothesViewPagerAdapter adapter=new ClothesViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);

        TabLayout tabLayout = findViewById(R.id.tablayout);
        tabLayout.setupWithViewPager(viewPager);
    }
}
