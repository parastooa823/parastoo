package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

import java.util.List;

import datamodel.Cloth;

public class ClothesAdapter extends RecyclerView.Adapter<ClothesAdapter.ClothesViewHolder> {

    private Context context;
    private List<Cloth> clothes;

    public ClothesAdapter(Context context, List<Cloth> clothes){

        this.context = context;
        this.clothes = clothes;
    }

    @NonNull
    @Override
    public ClothesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.layout_clothing_item,parent,false);
        return new ClothesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClothesViewHolder holder, int position) {
        Cloth cloth=clothes.get(position);
        holder.ClothImage.setImageDrawable(cloth.getImage());
        holder.ClothTitle.setText(cloth.getTitle());
        holder.ClothViewCount.setText(String.valueOf(cloth.getViewcount()));


    }

    @Override
    public int getItemCount() {
        return clothes.size();
    }


    public class ClothesViewHolder extends RecyclerView.ViewHolder {
        private ImageView ClothImage;
        private TextView ClothTitle;
        private TextView ClothViewCount;
     public  ClothesViewHolder(View itemView){
         super(itemView);
         ClothImage=(ImageView)itemView.findViewById(R.id.clothing_image);
         ClothTitle=(TextView)itemView.findViewById(R.id.clothing_title);
         ClothViewCount=(TextView)itemView.findViewById(R.id.clothing_viewcount_text);


     }
    }
}