package adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myapplication.R;

import java.util.List;

import datamodel.Post;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder> {


    private Context context;
    private List<Post> posts;

    public NewsAdapter(Context context, List<Post>Posts){


        this.context = context;
        posts = Posts;
    }


    @NonNull
    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(context).inflate(R.layout.layout_news,parent,false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        Post Post=posts.get(position);
        holder.newsImage.setImageDrawable(Post.getPostImage());
        holder.title.setText(Post.getTitle());
        holder.content.setText(Post.getContent());
        holder.date.setText(Post.getDate());




    }

    @Override
    public int getItemCount() {
        return posts.size();
    }

   public class NewsViewHolder extends RecyclerView.ViewHolder {
        private ImageView newsImage;
        private TextView title;
        private TextView content;
        private TextView date;


        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            newsImage=(ImageView)itemView.findViewById(R.id.item_image);
            title=(TextView)itemView.findViewById(R.id.news_title);
            content=(TextView)itemView.findViewById(R.id.news_content);
            date=(TextView)itemView.findViewById(R.id.news_date);


        }
    }

}


