package adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import view.fragment.ClothesFragment;

public class ClothesViewPagerAdapter extends FragmentPagerAdapter {
    public ClothesViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return ClothesFragment.newInstance();
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "مشاهده شده ما";
            case 1:
                return "پربازدیدترین ما";
            case 2:
                return "جدیدترین ما";
            default:
                return super.getPageTitle(position);
        }
    }
}
