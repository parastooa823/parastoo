package datamodel;

import android.graphics.drawable.Drawable;

public class Post {

    private int id;
    private Drawable PostImage;
    private String title;
    private String content;
    private String date;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Drawable getPostImage() {
        return PostImage;
    }

    public void setPostImage(Drawable postImage) {
        PostImage = postImage;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
