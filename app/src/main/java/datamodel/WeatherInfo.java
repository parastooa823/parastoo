package datamodel;

public class WeatherInfo {
    private String WeatherName;
    private String WeatherDescription;
    private float WindSpeed;
    private float winDegree;
    private int humidity;
    private float weatherTemprature;
    private float minTemprature;
    private float maxTemprature;
    private int pressure;


    public String getWeatherName() {
        return WeatherName;
    }

    public void setWeatherName(String weatherName) {
        WeatherName = weatherName;
    }

    public String getWeatherDescription() {
        return WeatherDescription;
    }

    public void setWeatherDescription(String weatherDescription) {
        WeatherDescription = weatherDescription;
    }

    public float getWindSpeed() {
        return WindSpeed;
    }

    public void setWindSpeed(float windSpeed) {
        WindSpeed = windSpeed;
    }

    public float getWinDegree() {
        return winDegree;
    }

    public void setWinDegree(float winDegree) {
        this.winDegree = winDegree;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public float getWeatherTemprature() {
        return weatherTemprature;
    }

    public void setWeatherTemprature(float weatherTemprature) {
        this.weatherTemprature = weatherTemprature;
    }

    public float getMinTemprature() {
        return minTemprature;
    }

    public void setMinTemprature(float minTemprature) {
        this.minTemprature = minTemprature;
    }

    public float getMaxTemprature() {
        return maxTemprature;
    }

    public void setMaxTemprature(float maxTemprature) {
        this.maxTemprature = maxTemprature;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }
}
